module.exports = {
	siteMetadata: {
		title: 'Ahmad Ainul Rizki',
		author: 'Ahmad Ainul Rizki',
		description: 'I\'m Ahmad — a digital craftsman focusing on front-end development & UI design. I work with companies around the world to make delightful digital products.',
		keywords: ['website', 'consulting', 'portfolio', 'blog', 'design', 'development'],
		siteUrl: 'https://tanahatas.me',
		email: 'ahmad@tanahatas.me',
		forHire: true
	},
	pathPrefix: '/',
	plugins: [
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/src/pages`,
				name: 'pages',
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/src/assets`,
				name: 'assets',
			},
		},
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				plugins: [
					{
						resolve: `gatsby-remark-images`,
						options: {
							backgroundColor: 'transparent',
							linkImagesToOriginal: false,
							maxWidth: 740
						},
					},
					{
						resolve: `gatsby-remark-responsive-iframe`,
						options: {
							wrapperStyle: `margin-bottom: 1.0725rem`,
						},
					},
					'gatsby-remark-autolink-headers',
					'gatsby-remark-external-links',
					'gatsby-remark-prismjs',
					'gatsby-remark-copy-linked-files',
					'gatsby-remark-smartypants',
				],
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: process.env.GOOGLE_ANALYTICS_ID,
				head: false,
			},
		},
		`gatsby-plugin-catch-links`,
		// `gatsby-plugin-feed`,
		`gatsby-plugin-offline`,
		`gatsby-plugin-react-helmet`,
		`gatsby-plugin-sitemap`,
		`gatsby-plugin-styled-components`,
		{
			resolve: `gatsby-plugin-nprogress`,
			options: {
				color: '#43bf4d',
				showSpinner: false
			},
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
			  icon: "src/favicon.png",
			},
		},
		{
			resolve: "gatsby-plugin-sentry",
			options: {
			  dsn: process.env.SENTRY_DSN_URL,
			  // Optional settings, see https://docs.sentry.io/clients/node/config/#optional-settings
			  environment: process.env.NODE_ENV,
			  enabled: (() => ["production", "stage"].indexOf(process.env.NODE_ENV) !== -1)()
			}
		},
		{
			resolve: `gatsby-plugin-hotjar`,
			options: {
			  id: process.env.HOTJAR_ID,
			  sv: 6
			},
		}
	],
}
