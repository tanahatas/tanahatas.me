## My Portfolio and Blog
*javascript junky / part-time open-sourcerer*

I'm a self-taught software engineer from Pekanbaru, ID with a background in design. I write empathetic code and build full-stack applications that people connect with directly. Regardless of the platform, solving difficult problems and building smooth user experiences is my jam.

*Check me out on [**GitHub**](https://github.com/tanahatas) and [**Instagram**](https://instagram.com/tanahatas)*

<strong>Live Website at <a href="https://tanahatas.me">tanahatas.me</a>.</strong>

---

### Tech Stack

- GatsbyJS v2
- ReactJS v16
- GraphQL
- webpack
- styled-components v4
- styled-system v4
- AWS Amplify

---

### Web Features

- Blog and Project category
- SEO: Open Graph tags, sitemap
- Syntax highlighting via PrismJS
- Constraint-based design via styled-system
- Dark Theme
- Traced SVG image preloaders
- Emojis by Twemoji
- Isolated `StaticQuery` per section

---

### Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run develop

# build for production and launch server
$ npm run build
$ npm run serve

# deploy to gh-pages
$ npm run generate
```

---

## I Love Coffee

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/C0C0125D1)
