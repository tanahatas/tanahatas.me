---
cover: ./cover.jpg
title: Ask Me Anythings
subtitle: Public Question
date: "2019-10-10"
category: note
draft: false
model: post
---

### Background

I've been inspired by [Kent C. Dodds](https://kentcdodds.com) lately to grow my online presence as a developer. My favorite thing about him is how he makes himself available to folks who are looking for advice or help.

So I created an AMA platform on [Github](https://github.com) similar to his to answer your questions. This idea originated from [Sindre Sorhus](https://sindresorhus.com) (whose website I love 💜), who also curates an [awesome](https://github.com/sindresorhus/awesome#readme) list of [AMAs](https://github.com/sindresorhus/amas#readme).

---

So what are you waiting for?

[ask me anythings](https://github.com/tanahatas/ask-me) 🍻
